package zadanie3.kalkulator.ofert;
public class Zadanie3KalkulatorOfert {
    public static void main(String[] args) {
        Client client1 = new Client();
        
        Article art1 = new Article("Kość słoniowa", new Price(500f));
        Article art2 = new Article("Telefon", new Price(77.5f), true, Promotion.Bon);
        Article art3 = new Article("Coś tam", new Price(10.20f), true, Promotion.PromotionalOfert);
        Article art4 = new Article("Kubek", new Price(3), true, Promotion.SpecialOfert);
        
        Cart cart1 = new Cart(client1);
        cart1.addArticle(art1, 3);
        cart1.addArticle(art2, 1);
         cart1.addArticle(art4, 1);
        cart1.addArticle(art3, 5);
        
        System.out.println(cart1.toString());
    }
    
}
