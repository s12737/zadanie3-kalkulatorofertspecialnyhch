package zadanie3.kalkulator.ofert;

import java.text.DecimalFormat;

public class Article {
    private String _name;
    private Price _price;
    private boolean _isInPromotion;
    private Promotion _promotion = null;
    
    
    public Article(String name, Price price) {
        this(name, price, false, null);
    }
    public Article(String name, Price price, boolean isInPromotion, Promotion promotion) {
        this._name = name;
        this._price = price;
        this._isInPromotion = isInPromotion;
        
        if(!this._isInPromotion)
            this._promotion = null;
        else {
            this._promotion = promotion;
            this._price = new Price(
                    price.getPrice() - (price.getPrice() * this._promotion.getValue()));
        }
    }
    
    
    public String getName() {
        return this._name;
    }
    public float getPrice() {
        return this._price.getPrice();
    }
    public boolean isInPromotion() {
        return this._isInPromotion;
    }
    public Promotion getPromotion() {
        return this._promotion;
    }
    
    @Override
    public String toString() {
        String str = "";
        
         DecimalFormat dFormat = new DecimalFormat("###.##");
        str += "{Name: " + this._name + ", price: " + dFormat.format(this._price.getPrice()) + ", Promotion: " +
                (this._isInPromotion ? "Yes" : "No") + " " +
                (this._isInPromotion ? this._promotion.toString() : "}");
        
        return str;
    }
    
}
