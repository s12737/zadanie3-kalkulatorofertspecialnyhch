package zadanie3.kalkulator.ofert;
public enum Promotion {
    Bon(0.10f), PromotionalOfert(0.25f), SpecialOfert(0.50f);
    
    private final float _value;
    
    private Promotion(float value) {
        this._value = value;
    }
    
    public float getValue() {
        return this._value;
    }
    
    @Override
    public String toString() {
        String str = "";
        
        str += "{" + this.name() + ", value: " + this._value + "}";
        
        return str;
    }
}
